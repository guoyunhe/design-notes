import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WindowComponent } from './window/window.component';
import { PhoneComponent } from './phone/phone.component';
import { TabletComponent } from './tablet/tablet.component';

@NgModule({
  declarations: [WindowComponent, PhoneComponent, TabletComponent],
  imports: [
    CommonModule
  ],
  exports: [WindowComponent]
})
export class SharedModule { }
